import App from './App.vue'
import './common/common.scss'
import Confirm from './components/common/Confirm/confirm.js'
import Toast from './components/common/Toast/toast.js'
const Vue = Yox.Vue
Vue.use(Confirm)
Vue.use(Toast)

Vue.config.productionTip = false
BJY.store.set('forceUseWss', true)

new Vue({
    render: (h) => h(App)
}).$mount('#app')
